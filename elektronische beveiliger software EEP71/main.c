/*  
 * ======== main.c ========
 */

/*
 * =========================== Includes ===========================
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <USB_config/descriptors.h>
#include "driverlib.h"
#include "hal.h"
// usb
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"
#include "USB_API/USB_CDC_API/UsbCdc.h"
#include "USB_app/usbConstructs.h"
#include "CDC_Commands.h"
// characteristics
#include "math.h"
// wifi
#include "simplelink/include/simplelink.h"
#include "sl_common.h"
#include "src/MQTTPacket.h"
/*
 *  ==================Defines DCO==================================
 */

// Defines DCO (clock) & timer ISR
#define COMPARE_VALUE 6000 // triggers every 1/4 ms
#define UCS_MCLK_DESIRED_FREQUENCY_IN_KHZ 24000 // 24 MHz
#define UCS_MCLK_FLLREF_RATIO 733

/*
 *  ==================Defines Flash================================
 */
// flash start memory
#define INFOB_START   (0x1900)      // INFO address in flash to which data is copied into
#define NUMBER_OF_BYTES   128       // Length of InfoB

/*
 *  ==================Defines wifi ================================
 */
#define SL_STOP_TIMEOUT        0xFF

/* Use bit 32:
 *      1 in a 'status_variable', the device has completed the ping operation
 *      0 in a 'status_variable', the device has not completed the ping operation
 */
#define STATUS_BIT_PING_DONE  31

#define HOST_NAME       "www.google.com"
#define SL_SSL_CA_CERT      "mqtt_ca.der"  /* CA certificate file ID */

/*
 * Values for below macros shall be modified for setting the 'Ping' properties
 */
#define PING_INTERVAL   1000    /* In msecs */
#define PING_TIMEOUT    3000    /* In msecs */
#define PING_PKT_SIZE   20      /* In bytes */
#define NO_OF_ATTEMPTS  3

/* Application specific status/error codes */
typedef enum{
    LAN_CONNECTION_FAILED = -0x7D0,        /* Choosing this number to avoid overlap with host-driver's error codes */
    INTERNET_CONNECTION_FAILED = LAN_CONNECTION_FAILED - 1,
    DEVICE_NOT_IN_STATION_MODE = INTERNET_CONNECTION_FAILED - 1,

    STATUS_CODE_MAX = -0xBB8
}e_AppStatusCodes;

#define IS_PING_DONE(status_variable)           GET_STATUS_BIT(status_variable, \
        STATUS_BIT_PING_DONE)

/*
 * =================== SWITCHDRIVER VARIABLES ======================
 */
// S.VALUES     (Set)
float S_Inom;   // Installed nominal current [A]
float S_Isc;    // Installed short ciruit current in x*Inom
float S_Ileak;  // Installed leakcurrent in [A]
int   S_Vmax;   // Installed maximum voltage [V]

// S.VALUES_A   (Set analog)
int   S_Inom_A; // Installed nominal current [Analog]
int   S_Isc_A;  // Installed short circuit current [Analog]
int   S_Ileak_A;// Installed leak current in [Analog]
int   S_Vmax_A; // Installed maximum voltage [Analog]

// M.VALUES_A   (Measure analog)
int   M_Vin_A;
int   M_Iin_A;  // Measured current [Analog]
int   M_Iout_A; // Measured current [Analog]
int   M_Ileak_A;// Measured leak current in [Analog]

// Factors      (A -> D and D -> A)
float AD_Current = 0.195312484; // Analog to digital current conversion factor
float DA_Current = 5.120000419; // Digital to analog current conversion factor
float AD_voltage = 0.014680988; // Digital to analog voltage conversion factor
float DA_Voltage = 68.11530365; // Analog to digital voltage conversion factor

// Current direction
uint16_t negative = 0;

// Factors      (Calculating current)
uint32_t startCurve_A = 0;
uint32_t stepCurve_A = 0;
uint64_t tempResult = 0;
uint64_t allowedValue = 0;

// Direct ADC values
uint16_t A0result;
uint16_t A1result;
uint16_t A2result;
uint16_t A3result;

// Circuitbreaker Tripcurve
uint32_t tripCurve[1024];
uint16_t buffer[1024];

// Temp float values for fixed point calculation
float tempStart;
float tempStep;

// usb real time plotting the adc
uint8_t adcValueReady = 0 ;

// check in of out
uint8_t inOut = 0 ;

// Calculate values switchdriver
void calcValues();

// ADC setup
void setupADC (void);
void setupTimerA_ISR (void);

/*
 * ======================== DCO setup ==========================
 */
// clock status
uint32_t clockValue = 0;
uint16_t status = 0 ;

// Clock
void SetVCoreUp (unsigned int level);
void setupDCO(void);

/*
 * ========================== USB ===============================
 */

// Function declarations
uint8_t retInString (char* string);

// sending data on COM port
void sendTxt(char *txt);
void sendNumber(uint16_t number);

// usb functies
void CDC_start_Menu(void);
void CDC_menu_settings(void);
void CDC_menu_stroom(void);
void CDC_menu_voltage(void);
void CDC_menu_wifi(void);
void USB_Constantcheck(void);
void USB_ConstantSend(void);

// function to Set flash in right variables
void flashSetVariables(void);

// Global flags set by events
volatile uint8_t bCDCDataReceived_event = FALSE; // Indicates data has been rx'ed

// check txt on the usb com port
const char *CDC_Checktxt();

// max lenght of the input
#define MAX_STR_LENGTH 64
char wholeString[MAX_STR_LENGTH] = ""; // Entire input str from last 'return'

/*
 * ========================== Flash ==============================
 */
// Data values of status, current, voltage and characteristics
uint16_t CAL_DATA[21];				// Values written in flash

// Writing and erasing
void write_infoB(void);
void erase_infoB(void);
void erase_mainFlash(void);
void write_mainFlash(void);

/*
 * =========================== Wifi ===============================
 */
_u32  g_Status = 0;
_u32  g_PingPacketsRecv = 0;
_u32  g_GatewayIP = 0;
_i16  sockID = 0;
_i16  ping_bit = 0;
_i16  send_bit = 0;

static _i32 configureSimpleLinkToDefaultState();
static _i32 establishConnectionWithAP();
static _i32 checkLanConnection();
static _i32 checkInternetConnection();
static _i32 initializeAppVariables();

static _i16 transport_getdata(unsigned char* buf, int count);
static _i32 mqtt_connect(SlSockAddr_t * addr, MQTTPacket_connectData * data);
static _i32 mqtt_disconnect();
static _i32 mqtt_ping();
static _i32 mqtt_publish(MQTTString topicString,unsigned char* payload);
static _i32 mqtt_subscribe(MQTTString * topicString, int * req_qos, int msgid);
int TLSConnectNetwork(SlSockAddr_t * sAddr, unsigned char sec_method, unsigned int cipher);
static void SimpleLinkPingReport(SlPingReport_t *pPingReport);

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    if(pWlanEvent == NULL)
    {

        return;
    }

    switch(pWlanEvent->Event)
    {
    case SL_WLAN_CONNECT_EVENT:
    {
        SET_STATUS_BIT(g_Status, STATUS_BIT_CONNECTION);
    }
    break;

    case SL_WLAN_DISCONNECT_EVENT:
    {
        slWlanConnectAsyncResponse_t*  pEventData = NULL;

        CLR_STATUS_BIT(g_Status, STATUS_BIT_CONNECTION);
        CLR_STATUS_BIT(g_Status, STATUS_BIT_IP_ACQUIRED);

        pEventData = &pWlanEvent->EventData.STAandP2PModeDisconnected;

        /* If the user has initiated 'Disconnect' request, 'reason_code' is SL_USER_INITIATED_DISCONNECTION */
        if(SL_WLAN_DISCONNECT_USER_INITIATED_DISCONNECTION == pEventData->reason_code)
        {

        }
        else
        {

        }
    }
    break;

    default:
    {

    }
    break;
    }
}
void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    if(pNetAppEvent == NULL)
    {

        return;
    }

    switch(pNetAppEvent->Event)
    {
    case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
    {
        SlIpV4AcquiredAsync_t *pEventData = NULL;

        SET_STATUS_BIT(g_Status, STATUS_BIT_IP_ACQUIRED);

        pEventData = &pNetAppEvent->EventData.ipAcquiredV4;
        g_GatewayIP = pEventData->gateway;
    }
    break;

    default:
    {

    }
    break;
    }
}
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{

}
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{

}
void SimpleLinkHttpServerCallback(SlHttpServerEvent_t *pHttpEvent,
                                  SlHttpServerResponse_t *pHttpResponse)
{

}

int wifi_start(void)
{
    int retVal = -1;

    /* identity data to connect with the broker */
    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
    data.clientID.cstring = "ID_1234";
    data.keepAliveInterval = 20;
    data.cleansession = 1;

    /* Set last will parameters */
    MQTTPacket_willOptions TheWill = MQTTPacket_willOptions_initializer;
    TheWill.message.cstring = "so long...";
    TheWill.topicName.cstring = "DCbeveiliger/#";
    data.will = TheWill;

    retVal = configureSimpleLinkToDefaultState();
    if(retVal < 0)
    {
        if (DEVICE_NOT_IN_STATION_MODE == retVal)

            return retVal;
    }

    /*
     * Assumption is that the device is configured in station mode already
     * and it is in its default state
     */
    retVal = sl_Start(0, 0, 0);
    if ((retVal < 0) ||
            (ROLE_STA != retVal) )
    {
        return retVal;
    }

    /* Connecting to WLAN AP */
    retVal = establishConnectionWithAP();
    if(retVal < 0)
    {

        return retVal;
    }

    sockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);

    /* host address */
    SlSockAddrIn_t addr;
    addr.sin_family = SL_AF_INET;
    addr.sin_port = sl_Htons(1883);
    addr.sin_addr.s_addr = sl_Htonl(SL_IPV4_VAL(192,168,1,136));

    retVal = mqtt_connect((SlSockAddr_t *)&addr, &data);
    if(retVal < 0)
    {
        sl_Close(sockID);
        return retVal;
    }

    return 0;

}

_i32 wifi_send(void)
{
    _i32 retVal  = -1;
    /* init topic to subscribe/publish to */
    MQTTString Voltage = MQTTString_initializer;
    Voltage.cstring = "DCbeveiliger/Voltage";

    MQTTString Current = MQTTString_initializer;
    Current.cstring = "DCbeveiliger/Current";

    MQTTString Power = MQTTString_initializer;
    Power.cstring = "DCbeveiliger/Power";

    /* payload to publish */
    unsigned char* pl_voltage = "x Volts";
    unsigned char* pl_current = "y Amps";
    unsigned char* pl_power = "z Watt";

    sprintf((char*)pl_voltage, "%d Volts", M_Vin_A);
    retVal = mqtt_publish(Voltage, pl_voltage);
    if(retVal < 0)
        return retVal;

    sprintf((char*)pl_current, "%d Amps", M_Iin_A);
    retVal = mqtt_publish(Current, pl_current);
    if(retVal < 0)
        return retVal;

    sprintf((char*)pl_power, "%d Watt", (M_Iin_A*M_Vin_A) );
    retVal = mqtt_publish(Power, pl_power);
    if(retVal < 0)
        return retVal;

    return 0;
}

void main (void)
{
    WDT_A_hold(WDT_A_BASE);             // Stop watchdog timer
    setupDCO();                         // clock setup

    /* return value for wifi functions */
    int retVal = -1;

    uint16_t *Flash_ptrA;               // Segment A pointer

    retVal = initializeAppVariables();          //init variables for wifi

    // reading flash memory
    Flash_ptrA = (uint16_t *)0x1900 ;           // Initialize Flash segment A ptr on memory 0x1900

    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN7); // Set Ledpin as output
    calcValues();

    uint16_t i;                 // for loop i

    for (i = 0; i < 20 ; i++)
    {
        CAL_DATA[i] = *Flash_ptrA++;            // Save flash memory in global Variables
        /* Cal_DATA[0] = status
         * CAL_DATA[1] = S_Inom
         * CAL_DATA[2] = S_Vmax
         * CAL_DATA[3] = S_Ileak
         * CAL_DATA[4] = handmatig of automatische
         * CAL_DATA[5] = if standaard a,b,c,d
         * CAL_DATA[6] = if automatic a
         * CAL_DATA[7] = if automatic b
         * CAL_DATA[8] = if automatic c
         * CAL_DATA[9] = if automatic S_Isc
         */
    }
    flashSetVariables() ;

    // baudrate max 460800
    init_CDC()  ;			// init CDC for USB settings

    /* start up the wifi module, connect to AP and server  */
    retVal = wifi_start();
    if(retVal < 0)
    {
        sl_Stop(SL_STOP_TIMEOUT);
    }
    setupADC();             // SETUP 4x ADC_12
    setupTimerA_ISR();      // ISR timer

    while(1)
    {
        if(ping_bit == 1)
        {
            mqtt_ping();
            ping_bit = 0;
        }
        if(send_bit == 1)
        {
            wifi_send();
            send_bit = 0;
        }
    }

    // set comport received data on false
    bCDCDataReceived_event = FALSE ;
    //CAL_DATA[0]	= 0xFFF1 ;

    while(1)
    {
        /* when system is active CAL_DATA[0] is 0xFFF1
         * CAL_DATA[0] is on initalisation 0XFFFF
         * CAL_DATA[0] = 0xFFF3 short circuit schrijf daarna write_infoB();
         * CAL_DATA[0] = 0xFFF2 System deactivated schrijf daarna write_infoB();
         */
        if ( CAL_DATA[0] == (uint16_t) 0xFFF1 )
        {
        	USB_Constantcheck();
            if ( adcValueReady == 1 )
            {
                USB_ConstantSend();
            }
        }
        else
        {
            // disable the ISR interrupt
            Timer_A_disableInterrupt(TIMER_A1_BASE);
            Timer_A_stop(TIMER_A1_BASE);
            // disable the adc interrupt
            ADC12_A_disable(ADC12_A_BASE);

            // short circuit
            if ( CAL_DATA[0] == 0xFFF3 )
            {
                /*send message to server */
                MQTTString goobye_Tpc = MQTTString_initializer;
                goobye_Tpc.cstring = "DCbeveiliger/#";

                mqtt_publish(goobye_Tpc, (char*)"Shortcircuit: Device switching off.");
            	/*disconnect form server and turn off wifi-module to preserve power */
                mqtt_disconnect();
                sl_Stop(SL_STOP_TIMEOUT);
            }

            // when system deactivated CDC_start_Menu has to wait for COM port connection
            CDC_start_Menu();
        }
    }
}

/*  
 * ======================== UNMI_ISR ========================
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(UNMI_VECTOR))) UNMI_ISR (void)
#else
#error Compiler not found!
#endif
{
    switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG))
    {
    case SYSUNIV_NONE:
        __no_operation();
        break;
    case SYSUNIV_NMIIFG:
        __no_operation();
        break;
    case SYSUNIV_OFIFG:
        UCS_clearFaultFlag(UCS_XT2OFFG);
        UCS_clearFaultFlag(UCS_DCOFFG);
        SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
        break;
    case SYSUNIV_ACCVIFG:
        __no_operation();
        break;
    case SYSUNIV_BUSIFG:
        // If the CPU accesses USB memory while the USB module is
        // suspended, a "bus error" can occur.  This generates an NMI.  If
        // USB is automatically disconnecting in your software, set a
        // breakpoint here and see if execution hits it.  See the
        // Programmer's Guide for more information.
        SYSBERRIV = 0; //clear bus error flag
        USB_disable(); //Disable
    }
}
/* This function sends text to the COM port
 * max 64 characters
 */
void sendTxt(char *txt)
{
    char outString2[64] = "";

    strcpy(outString2, txt );
    // Send the response over USB
    USBCDC_sendDataInBackground((uint8_t*)outString2, strlen(outString2),CDC0_INTFNUM,0);
}
/* sendNumber sends number to the COM port
 *
 */
void sendNumber(uint16_t number)
{
    uint8_t i = 0 ;
    uint16_t placeValue ;
    char outString2[64] = "" ;
    char outString3[5] ;

    for ( i = 0 ; i < 5 ; i++ )
    {
        placeValue = number % 10;
        number = number / 10;

        switch(placeValue)
        {
        case 1 :
            outString3[4-i] = '1' ;
            break ;
        case 2 :
            outString3[4-i] = '2' ;
            break ;
        case 3 :
            outString3[4-i] = '3' ;
            break ;
        case 4 :
            outString3[4-i] = '4' ;
            break ;
        case 5 :
            outString3[4-i] = '5' ;
            break ;
        case 6 :
            outString3[4-i] = '6' ;
            break ;
        case 7 :
            outString3[4-i] = '7' ;
            break ;
        case 8 :
            outString3[4-i] = '8' ;
            break ;
        case 9 :
            outString3[4-i] = '9' ;
            break ;
        default   :
            outString3[4-i] = '0' ;
            break ;
        }
    }
    strcpy(outString2, outString3 );
    // send data
    USBCDC_sendDataInBackground((uint8_t*)outString2, strlen(outString2),CDC0_INTFNUM,0);
}
/* This function is the menu of the system
 * This menu opens in putty :
 * Speed on 9600
 * Data bits 8
 * stop bits 1
 * parity none
 * flow Control XON/XOFF
 */
void CDC_start_Menu( void )
{
    const char * vergelijken = "" ;
    uint8_t i = 0 ;

    while( ! ( USB_getConnectionState() == ST_ENUM_ACTIVE )) ;
    // Prepare the outgoing string
    sendTxt("\033[2Jstatus system: ");

    switch (CAL_DATA[0])
    {
    case 0xFFFF :
        sendTxt("new system\r\n");
        break ;
    case 0xFFF2 :
        sendTxt("system deactivated\r\n");
        break ;
    case 0xFFF3 :
        sendTxt("short circuit\r\n");
        break ;
    case 0xFFF1 :
        sendTxt("active system\r\n");
        break;
    default :
        sendTxt("error\r\n");
        break ;
    }
    sendTxt("1.\tsettings\n2.\tstart\r");
    if ( CAL_DATA[0] == 0xFFF3 )
    {
        sendTxt("3.\tshort circuit\r");
    }
    vergelijken = ((const char *)CDC_Checktxt()) ;
    // system willn't start until the start command is pressed
    if (! strcmp("start" , vergelijken ) )
    {
        if ( CAL_DATA[5] < 5 || CAL_DATA[15] < 5)
        {
            sendTxt("system will turn on in 5 seconds\n");
            for ( i = 0 ; i < 6 ; i++)
            {
                __delay_cycles(8000000);
                CAL_DATA[0] = 0xFFF1 ;
                write_infoB();
                sendNumber(5-i);
                sendTxt("\n");
            }
            sendTxt("System status active\n");

            flashSetVariables();

            /* turn on  */
            setupADC();             // SETUP 4x ADC_12
            setupTimerA_ISR();      // ISR timer

            return ;
        }
        else
        {
            sendTxt("Unable to start settings not ready\r");
            __delay_cycles(250000);
        }

    }
    else if (! strcmp("settings" , vergelijken ) )
    {
        CDC_menu_settings();
    }
    else if (! strcmp("short circuit" , vergelijken ) )
    {
        sendTxt("\n");
        for ( i = 0 ; i < 100 ; i++)
        {
            sendNumber(i);
            sendTxt("\n");
        }
        sendTxt("\nenter oke\n");
        while (! strcmp("voltage" , ((const char *)CDC_Checktxt()) ) )  ;
    }
    else
    {
        sendTxt("Wrong command\r");
        __delay_cycles(500000);
    }
}
/* This function is the settings menu
 *
 */
void CDC_menu_settings(void)
{
    const char * vergelijken = "" ;
    sendTxt("\033[2Jsettings menu\n1.\tcurrent in/out\n2.\tvoltage in/out\nexit\r");
    vergelijken = ((const char *)CDC_Checktxt()) ;
    while(1)
    {
        if (! strcmp("exit" , vergelijken ) )
        {
            return ;
        }
        else if (! strcmp("current in" , vergelijken ) )
        {
        	inOut = 0 ;
            CDC_menu_stroom();
        }
        else if (! strcmp("current out" , vergelijken ) )
        {
        	inOut = 1 ;
            CDC_menu_stroom();
        }
        else if (! strcmp("voltage in" , vergelijken ) )
        {
            inOut = 0 ;
        	CDC_menu_voltage();

        }
        else if (! strcmp("voltage out" , vergelijken ) )
        {
        	inOut = 1 ;
            CDC_menu_voltage();
        }
        else
        {
            sendTxt("\033[2JWrong command\r");
        }

        sendTxt("\033[2Jsettings menu\n1.\tcurrent in/out\n2.\tvoltage in/out\nexit\r");
        vergelijken = ((const char *)CDC_Checktxt()) ;

    }
}
/* This function is the current menu
 *
 */
void CDC_menu_stroom(void)
{
    const char * vergelijken = "" ;
    int enable = 0 ;
    int newValue = 0 ;

    sendTxt("\033[2Jcurrent settings:\t");
    if ( inOut == 0 )
    {
    	sendTxt("input");
    }
    else
    {
    	sendTxt("output");
    }
    sendTxt("\r1.\tnominaal\t\t");
    sendTxt("current value:\t");
    if ( inOut == 0 )
    {
    	sendNumber( (uint16_t)CAL_DATA[1] ) ;
    }
    else
    {
    	sendNumber( (uint16_t)CAL_DATA[11] ) ;
    }
    sendTxt("\r2.\tcharacteristics\t\t");
    sendTxt("current Type:\t");
    if ( inOut == 0 )
    {
        switch (CAL_DATA[5])
        {
        case 0 :
            sendTxt("A");
            break;
        case 1 :
            sendTxt("B");
            break;
        case 2 :
            sendTxt("C");
            break;
        case 3 :
            sendTxt("D");
            break ;
        case 4 :
            sendNumber(CAL_DATA[6]);
            sendTxt("*x^");
            sendNumber(CAL_DATA[7]);
            sendTxt("+");
            sendNumber(CAL_DATA[8]);
            break ;
        default :
        	break ;
        }
    }
    else
    {
        switch (CAL_DATA[15])
        {
        case 0 :
            sendTxt("A");
            break;
        case 1 :
            sendTxt("B");
            break;
        case 2 :
            sendTxt("C");
            break;
        case 3 :
            sendTxt("D");
            break ;
        case 4 :
            sendNumber(CAL_DATA[16]);
            sendTxt("*x^");
            sendNumber(CAL_DATA[17]);
            sendTxt("+");
            sendNumber(CAL_DATA[18]);
            break ;
        default :
        	break ;
        }
    }
    sendTxt("\r3.\tleak\t\t\t");
    sendTxt("current value:\t");
    if ( inOut == 0 )
    {
        sendNumber( (uint16_t)CAL_DATA[3] ) ;
    }
    else
    {
        sendNumber( (uint16_t)CAL_DATA[13] ) ;
    }

    sendTxt("\nexit\r");

    vergelijken = ((const char *)CDC_Checktxt()) ;
    while( 1 )
    {
        if (! strcmp("exit" , vergelijken ) )
        {
            return ;
        }
        else if ( ! strcmp("nominaal" , vergelijken ) )
        {
            sendTxt("Value between 0A and 500A\t");
            sendTxt("current value:\t");
            if ( inOut == 0 )
            {
            	sendNumber( (uint16_t)CAL_DATA[1] ) ;
            }
            else
            {
            	sendNumber( (uint16_t)CAL_DATA[11] ) ;
            }
            sendTxt("\r");
            while(enable == 0)
            {
                newValue = atoi(CDC_Checktxt()) ;
                if( newValue < 501 )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                        CAL_DATA[1] = newValue;
                    }
                    else
                    {
                    	CAL_DATA[11] = newValue;
                    }

                    write_infoB() ;
                }
                else
                {
                    sendTxt("wrong value\r");
                }
            }
            enable = 0 ;
        }
        else if ( ! strcmp("leak" , vergelijken ) )
        {
            sendTxt("Value between 0A and 30mA\t");
            sendTxt("current value:\t");
            if ( inOut == 0 )
            {
            	sendNumber( (uint16_t)CAL_DATA[3] ) ;
            }
            else
            {
            	sendNumber( (uint16_t)CAL_DATA[13] ) ;
            }

            sendTxt("\r");
            while(enable == 0)
            {
                newValue = atoi(CDC_Checktxt()) ;
                if( newValue < 31 )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                    	CAL_DATA[3] = newValue;
                    }
                    else
                    {
                    	CAL_DATA[13] = newValue;
                    }
                    write_infoB() ;
                }
                else
                {
                    sendTxt("wrong value\r");
                }
            }
            enable = 0 ;
        }
        else if (! strcmp("characteristics" , vergelijken ) )
        {
            sendTxt("send characteristics type A, B, C, D or new \n");
            sendTxt("current type:\t");
            if ( inOut == 0 )
            {
                switch (CAL_DATA[5])
                {
                case 0 :
                    sendTxt("A");
                    break;
                case 1 :
                    sendTxt("B");
                    break;
                case 2 :
                    sendTxt("C");
                    break;
                case 3 :
                    sendTxt("D");
                    break ;
                case 4 :
                    sendNumber(CAL_DATA[6]);
                    sendTxt("*x^");
                    sendNumber(CAL_DATA[7]);
                    sendTxt("+");
                    sendNumber(CAL_DATA[8]);
                    break ;
                default :
                	break ;
                }
            }
            else
            {
                switch (CAL_DATA[15])
                {
                case 0 :
                    sendTxt("A");
                    break;
                case 1 :
                    sendTxt("B");
                    break;
                case 2 :
                    sendTxt("C");
                    break;
                case 3 :
                    sendTxt("D");
                    break ;
                case 4 :
                    sendNumber(CAL_DATA[16]);
                    sendTxt("*x^");
                    sendNumber(CAL_DATA[17]);
                    sendTxt("+");
                    sendNumber(CAL_DATA[18]);
                    break ;
                default :
                	break ;
                }

            }
            sendTxt("\r");
            while(enable == 0)
            {
                vergelijken = ((const char *)CDC_Checktxt()) ;
                if(! strcmp("A" , vergelijken ) )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                    	CAL_DATA[5] = 0;
                    }
                    else
                    {
                    	CAL_DATA[15] = 0;
                    }

                    write_infoB() ;

                }
                else if ( ! strcmp("B" , vergelijken ) )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                    	CAL_DATA[5] = 1;
                    }
                    else
                    {
                    	CAL_DATA[15] = 1;
                    }
                    write_infoB() ;
                }
                else if ( ! strcmp("C" , vergelijken ) )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                    	CAL_DATA[5]  = 2;
                    }
                    else
                    {
                    	CAL_DATA[15] = 2;
                    }
                    write_infoB() ;
                }
                else if ( ! strcmp("D" , vergelijken ) )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    if ( inOut == 0 )
                    {
                    	CAL_DATA[5] = 3;
                    }
                    else
                    {
                    	CAL_DATA[15] = 3;
                    }
                    write_infoB() ;
                }
                else if ( ! strcmp("new" , vergelijken ) )
                {


                    while(enable == 0)
                    {
                    	sendTxt(" a * x^b + c\tsend a\n");
                        newValue = atoi(CDC_Checktxt()) ;
                        if( newValue < 500 )
                        {
                        	sendTxt("good, exit\r");
                        	enable = 1 ;
                        	if ( inOut == 0 )
                        	{
                        		CAL_DATA[5] = 4 ;
                        		CAL_DATA[6] = newValue;
                        	}
                        	else
                        	{
                        		CAL_DATA[15] = 4 ;
                        		CAL_DATA[16] = newValue;
                        	}
                        }
                        else
                        {
                        	sendTxt("wrong value\r");
                        }
                    }
                    enable = 0 ;
                    while(enable == 0)
                    {
                    	sendTxt(" a * x^b + c\tsend b\n");
                        newValue = atoi(CDC_Checktxt()) ;
                        if( newValue < 500 )
                        {
                        	sendTxt("good, exit\r");
                        	enable = 1 ;
                        	if ( inOut == 0 )
                        	{
                        		CAL_DATA[7] = newValue;
                        	}
                        	else
                        	{
                        		CAL_DATA[17] = newValue;
                        	}
                        }
                        else
                        {
                        	sendTxt("wrong value\r");
                        }
                    }
                    enable = 0 ;
                    while(enable == 0)
                    {
                    	sendTxt(" a * x^b + c\tsend c\n");
                        newValue = atoi(CDC_Checktxt()) ;
                        if( newValue < 500 )
                        {
                        	sendTxt("good, exit\r");
                        	enable = 1 ;
                        	if ( inOut == 0 )
                        	{
                        		CAL_DATA[8] = newValue;
                        	}
                        	else
                        	{
                        		CAL_DATA[18] = newValue;
                        	}

                        }
                        else
                        {
                        	sendTxt("wrong value\r");
                        }
                    }
                	sendTxt("good, exit\r");
                    enable = 1 ;
                    write_infoB() ;
                }
                else
                {
                    sendTxt("wrong value\r");
                }
            }
            enable = 0 ;
        }
        else
        {
            sendTxt("\033[2JWrong command\r");
        }

        sendTxt("\033[2Jcurrent settings:\t");
        if ( inOut == 0 )
        {
        	sendTxt("input");
        }
        else
        {
        	sendTxt("output");
        }
        sendTxt("\r1.\tnominaal\t\t");
        sendTxt("current value:\t");
        if ( inOut == 0 )
        {
        	sendNumber( (uint16_t)CAL_DATA[1] ) ;
        }
        else
        {
        	sendNumber( (uint16_t)CAL_DATA[11] ) ;
        }
        sendTxt("\r2.\tcharacteristics\t\t");
        sendTxt("current Type:\t");
        if ( inOut == 0 )
        {
            switch (CAL_DATA[5])
            {
            case 0 :
                sendTxt("A");
                break;
            case 1 :
                sendTxt("B");
                break;
            case 2 :
                sendTxt("C");
                break;
            case 3 :
                sendTxt("D");
                break ;
            case 4 :
                sendNumber(CAL_DATA[6]);
                sendTxt("*x^");
                sendNumber(CAL_DATA[7]);
                sendTxt("+");
                sendNumber(CAL_DATA[8]);
                break ;
            default :
            	break ;
            }
        }
        else
        {
            switch (CAL_DATA[15])
            {
            case 0 :
                sendTxt("A");
                break;
            case 1 :
                sendTxt("B");
                break;
            case 2 :
                sendTxt("C");
                break;
            case 3 :
                sendTxt("D");
                break ;
            case 4 :
                sendNumber(CAL_DATA[16]);
                sendTxt("*x^");
                sendNumber(CAL_DATA[17]);
                sendTxt("+");
                sendNumber(CAL_DATA[18]);
                break ;
            default :
            	break ;
            }
        }
        sendTxt("\r3.\tleak\t\t\t");
        sendTxt("current value:\t");
        if ( inOut == 0 )
        {
            sendNumber( (uint16_t)CAL_DATA[3] ) ;
        }
        else
        {
            sendNumber( (uint16_t)CAL_DATA[13] ) ;
        }

        sendTxt("\nexit\r");

        vergelijken = ((const char *)CDC_Checktxt()) ;
    }

}
/* This function is the voltage menu
 *
 */
void CDC_menu_voltage(void)
{
    const char * vergelijken = "" ;
    int enable = 0 ;
    int newValue = 0 ;
    uint8_t systemExit = 0 ;

    sendTxt("\033[2Jvoltage settings: \t");
    if ( inOut == 0 )
    {
    	sendTxt("input");
    }
    else
    {
    	sendTxt("output");
    }
    sendTxt("\r1.\tmax\t\t\t");
    sendTxt("current value:\t");
    if ( inOut == 0 )
    {
    	sendNumber( (uint16_t)CAL_DATA[2] ) ;
    }
    else
    {
    	sendNumber( (uint16_t)CAL_DATA[12] ) ;
    }

    sendTxt("\nexit\r");

    vergelijken = ((const char *)CDC_Checktxt()) ;
    while( systemExit == 0 )
    {

        if (! strcmp("exit" , vergelijken ) )
        {
            return ;
        }
        else if ( ! strcmp("max" , vergelijken ) )
        {
            sendTxt("Value between 0V and 50V\t");
            sendTxt("current value:\t");
            if ( inOut == 0 )
            {
            	 sendNumber( (uint16_t)CAL_DATA[2] ) ;
            }
            else
            {
            	 sendNumber( (uint16_t)CAL_DATA[12] ) ;
            }

            sendTxt("\r");

            while(enable == 0)
            {
                newValue = atoi(CDC_Checktxt()) ;
                if( newValue < 51 )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;

                    if ( inOut == 0 )
                    {
                    	CAL_DATA[2] = newValue;
                    }
                    else
                    {
                    	CAL_DATA[12] = newValue;
                    }
                    write_infoB() ;
                }
                else
                {
                    sendTxt("wrong value\r");
                }
            }
        }
        else
        {
            sendTxt("Wrong command\r");
        }
        sendTxt("\033[2Jvoltage settings: \t");
        if ( inOut == 0 )
        {
        	sendTxt("input");
        }
        else
        {
        	sendTxt("output");
        }
        sendTxt("\r1.\tmax\t\t\t");
        sendTxt("current value:\t");
        if ( inOut == 0 )
        {
        	sendNumber( (uint16_t)CAL_DATA[2] ) ;
        }
        else
        {
        	sendNumber( (uint16_t)CAL_DATA[12] ) ;
        }

        sendTxt("\nexit\r");

        vergelijken = ((const char *)CDC_Checktxt()) ;
    }
}
/* This function is the wifi menu
 *
 */
void CDC_menu_wifi( void )
{
    const char * vergelijken = "" ;
    int enable = 0 ;
    int newValue = 0 ;
    uint8_t systemExit = 0 ;

    sendTxt("\033[2Jwifi settings: \t");
    sendTxt("\r1.\tid\t\t\t");
    sendTxt("current id:\t");
    sendNumber( (uint16_t)CAL_DATA[4] ) ;
    sendTxt("\nexit\r");

    vergelijken = ((const char *)CDC_Checktxt()) ;
    while( systemExit == 0 )
    {
        if (! strcmp("exit" , vergelijken ) )
        {
            return ;
        }
        else if ( ! strcmp("id" , vergelijken ) )
        {
            sendTxt("id in XXXX \t");
            sendTxt("current value:\t");
            sendNumber( (uint16_t)CAL_DATA[4] ) ;
            sendTxt("\r");

            while(enable == 0)
            {
                newValue = atoi(CDC_Checktxt()) ;
                if( newValue < 9999 )
                {
                    sendTxt("good, exit\r");
                    enable = 1 ;
                    CAL_DATA[6] = newValue;
                    write_infoB() ;

                }
                else
                {
                    sendTxt("wrong value\r");
                }
            }
        }
        else
        {
            sendTxt("Wrong command\r");
        }
        sendTxt("\033[2Jwifi settings: \t");
        sendTxt("\r1.\tid\t\t\t");
        sendTxt("current id:\t");
        sendNumber( (uint16_t)CAL_DATA[4] ) ;
        sendTxt("\nexit\r");
        vergelijken = ((const char *)CDC_Checktxt()) ;
    }


}
/* This function checks if there is txt on the COM PORT
 *
 */
const char *CDC_Checktxt()
{
    char wholeString2[64] = "";
    while(1)
    {
        // Check the USB state and directly main loop accordingly
        switch (USB_getConnectionState())
        {
        // This case is executed while your device is enumerated on the
        // USB host
        case ST_ENUM_ACTIVE:

            // Enter LPM0 (can't do LPM3 when active)
            __bis_SR_register(LPM0_bits + GIE);
            _NOP();
            // Exit LPM on USB receive and perform a receive operation

            // If true, some data is in the buffer; begin receiving a cmd
            if (bCDCDataReceived_event)
            {
                // Holds the new addition to the string
                char pieceOfString[MAX_STR_LENGTH] = "";
                // Holds the outgoing string
                //char outString[MAX_STR_LENGTH] = "";
                // Add bytes in USB buffer to the string
                USBCDC_receiveDataInBuffer((uint8_t*)pieceOfString, MAX_STR_LENGTH, CDC0_INTFNUM); // Get the next piece of the string
                // Append new piece to the whole
                strcat(wholeString2,pieceOfString);
                // Echo back the characters received
                USBCDC_sendDataInBackground((uint8_t*)pieceOfString, strlen(pieceOfString),CDC0_INTFNUM,0);
                // Has the user pressed return yet?
                if (retInString(wholeString2))
                {
                    const char *waarde = wholeString2 ;
                    bCDCDataReceived_event = FALSE;
                    return waarde ;
                }

            }
            break;
            // These cases are executed while your device is disconnected from
            // the host (meaning, not enumerated); enumerated but suspended
            // by the host, or connected to a powered hub without a USB host
            // present.
        case ST_PHYS_DISCONNECTED:
        case ST_ENUM_SUSPENDED:
        case ST_PHYS_CONNECTED_NOENUM_SUSP:
            //Turn off LED P1.0
            GPIO_setOutputLowOnPin(LED_PORT, LED_PIN);
            __bis_SR_register(LPM3_bits + GIE);
            _NOP();
            break;

            // The default is executed for the momentary state
            // ST_ENUM_IN_PROGRESS.  Usually, this state only last a few
            // seconds.  Be sure not to enter LPM3 in this state; USB
            // communication is taking place here, and therefore the mode must
            // be LPM0 or active-CPU.
        case ST_ENUM_IN_PROGRESS:
        default:;
        }
    }

}
// sends constant data to usb if it is placed
void USB_Constantcheck(void)
{
    if( USB_getConnectionState() == ST_ENUM_ACTIVE )
    {
        if(bCDCDataReceived_event)
        {
            // disable system
            CAL_DATA[0] = 0xFFF2 ;
        }
        else
        {}
    }
    else
    {}
}

void USB_ConstantSend(void)
{
    if( USB_getConnectionState() == ST_ENUM_ACTIVE )
    {
        sendNumber( (uint16_t)M_Iin_A ) ;
        sendTxt("\n");
        adcValueReady = 0 ;
    }
    else
    {}
}

//******************************* Flash write  ***************************************
void write_infoB( void )
{
    uint16_t i = 0 ;
    uint16_t *memoryAdres, *memorySave ;
    memoryAdres = (uint16_t *)0x1900 ;

    // erase flash
    erase_infoB();

    //Flash Write
    for( i = 0 ; i < 20 ; i++)
    {
        memorySave = &CAL_DATA[i] ;
        FlashCtl_write16( (uint16_t*) memorySave, (uint16_t*) memoryAdres++, 1);
    }

}

//******************************* Flash erase ***************************************
void erase_infoB(void)
{
    uint8_t status;

    // Erase INFOB
    FlashCtl_eraseSegment( (uint8_t*)INFOB_START );
    status = FlashCtl_getStatus	(FLASHCTL_READY_FOR_NEXT_WRITE);
    while (status != FLASHCTL_READY_FOR_NEXT_WRITE);

}

// set variables for flash to menu
void flashSetVariables(void)
{
    /* Cal_DATA[0] = status							(IN)

     * CAL_DATA[1] = S_Inom							(IN)
     * CAL_DATA[2] = S_Vmax							(IN)
     * CAL_DATA[3] = S_Ileak						(IN)
     * CAL_DATA[4] = 								(IN)
     * CAL_DATA[5] = if standaard a,b,c,d			(IN)
     * CAL_DATA[6] = if automatic a 				(IN)
     * CAL_DATA[7] = if automatic b					(IN)
     * CAL_DATA[8] = if automatic c					(IN)
     * CAL_DATA[9] = if automatic S_Isc				(IN)
     * CAL_DATA[10]= direction in or out
     *
     * CAL_DATA[11] = S_Inom						(OUT)
     * CAL_DATA[12] = S_Vmax						(OUT)
     * CAL_DATA[13] = S_Ileak						(OUT)
     * CAL_DATA[14] = 								(OUT)
     * CAL_DATA[15] = if standaard a,b,c,d			(OUT)
     * CAL_DATA[16] = if automatic a 				(OUT)
     * CAL_DATA[17] = if automatic b				(OUT)
     * CAL_DATA[18] = if automatic c				(OUT)
     * CAL_DATA[19] = if automatic S_Isc			(OUT)
     */

		// direction is in
		S_Inom  = CAL_DATA[1] ;
		S_Vmax  = CAL_DATA[2] ;
		S_Ileak = CAL_DATA[3] ;
		S_Isc 	= CAL_DATA[9] ;

		S_Inom  = CAL_DATA[11] ;
		S_Vmax  = CAL_DATA[12] ;
		S_Ileak = CAL_DATA[13] ;


}


/* Snelheid systeem veranderen
 *
 */
void setupDCO(void)
{
    //Set VCore = 1 for 12MHz clock
    PMM_setVCore(PMM_CORE_LEVEL_3);

    //Set DCO FLL reference = REFO
    UCS_initClockSignal(
        UCS_FLLREF,
        UCS_REFOCLK_SELECT,
        UCS_CLOCK_DIVIDER_1
        );
    //Set ACLK = REFO
    UCS_initClockSignal(
        UCS_ACLK,
        UCS_REFOCLK_SELECT,
        UCS_CLOCK_DIVIDER_1
        );

    //Set Ratio and Desired MCLK Frequency  and initialize DCO
    UCS_initFLLSettle(
        UCS_MCLK_DESIRED_FREQUENCY_IN_KHZ,
        UCS_MCLK_FLLREF_RATIO
        );

    // Enable global oscillator fault flag
    //SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
    //SFR_enableInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);

    // Enable global interrupt
    __bis_SR_register(GIE);

    //Verify if the Clock settings are as expected
    clockValue = UCS_getSMCLK();
    clockValue = UCS_getMCLK();
    clockValue = UCS_getACLK();
}
/* Snelheid systeem veranderen
 *
 */
void SetVCoreUp (unsigned int level)
{
    // Open PMM registers for write access
    PMMCTL0_H = 0xA5;
    // Set SVS/SVM high side new level
    SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
    // Set SVM low side to new level
    SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
    // Wait till SVM is settled
    while ((PMMIFG & SVSMLDLYIFG) == 0);
    // Clear already set flags
    PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
    // Set VCore to new level
    PMMCTL0_L = PMMCOREV0 * level;
    // Wait till new level reached
    if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
    // Set SVS/SVM low side to new level
    SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
    // Lock PMM registers for write access
    PMMCTL0_H = 0x00;
}


void setupADC (void)
{
    //Enable A/D channel inputs
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P6,
        GPIO_PIN0 + GPIO_PIN1 + GPIO_PIN2 +
        GPIO_PIN3 + GPIO_PIN4 + GPIO_PIN5 +
        GPIO_PIN6
        );

    //Initialize the ADC12_A Module
    /*
     * Base address of ADC12_A Module
     * Use internal ADC12_A bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC12_A_init(ADC12_A_BASE,
        ADC12_A_SAMPLEHOLDSOURCE_SC,
        ADC12_A_CLOCKSOURCE_SMCLK,
        ADC12_A_CLOCKDIVIDER_1
        );

    ADC12_A_enable(ADC12_A_BASE);

    /*
     * Base address of ADC12_A Module
     * For memory buffers 0-7 sample/hold for 256 clock cycles
     * For memory buffers 8-15 sample/hold for 4 clock cycles (default)
     * Enable Multiple Sampling
     */
    ADC12_A_setupSamplingTimer(ADC12_A_BASE,
        ADC12_A_CYCLEHOLD_4_CYCLES,
        ADC12_A_CYCLEHOLD_256_CYCLES, // 8 is default
        ADC12_A_MULTIPLESAMPLESENABLE);

    //Configure Memory Buffers
    /*
     * Base address of the ADC12_A Module
     * Configure memory buffer 0
     * Map input A0 to memory buffer 0
     * Vref+ = AVcc
     * Vref- = AVss
     * Memory buffer 0 is not the end of a sequence
     */
    ADC12_A_configureMemoryParam param0 = {0};
    param0.memoryBufferControlIndex = ADC12_A_MEMORY_0;
    param0.inputSourceSelect = ADC12_A_INPUT_A4;
    param0.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param0.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param0.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE ,&param0);

    /*
     * Base address of the ADC12_A Module
     * Configure memory buffer 1
     * Map input A1 to memory buffer 1
     * Vref+ = AVcc
     * Vref- = AVss
     * Memory buffer 1 is not the end of a sequence
     *
     */
    ADC12_A_configureMemoryParam param1 = {0};
    param1.memoryBufferControlIndex = ADC12_A_MEMORY_1;
    param1.inputSourceSelect = ADC12_A_INPUT_A5;
    param1.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param1.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param1.endOfSequence = ADC12_A_NOTENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE ,&param1);
    /*
     * Base address of the ADC12_A Module
     * Configure memory buffer 2
     * Map input A2 to memory buffer 2
     * Vref+ = AVcc
     * Vref- = AVss
     * Memory buffer 2 is not the end of a sequence
     */
    ADC12_A_configureMemoryParam param2 = {0};
    param2.memoryBufferControlIndex = ADC12_A_MEMORY_2;
    param2.inputSourceSelect = ADC12_A_INPUT_A6;
    param2.positiveRefVoltageSourceSelect = ADC12_A_VREFPOS_AVCC;
    param2.negativeRefVoltageSourceSelect = ADC12_A_VREFNEG_AVSS;
    param2.endOfSequence = ADC12_A_ENDOFSEQUENCE;
    ADC12_A_configureMemory(ADC12_A_BASE ,&param2);

    //Enable memory buffer 3 interrupt
    ADC12_A_clearInterrupt(ADC12_A_BASE,
        ADC12IFG3);
    ADC12_A_enableInterrupt(ADC12_A_BASE,
        ADC12IE3);

    //Enable/Start first sampling and conversion cycle
    /*
     * Base address of ADC12_A Module
     * Start the conversion into memory buffer 0
     * Use the repeated sequence of channels
     */

    return;
}


void setupTimerA_ISR (void)
{
    //Stop WDT
    WDT_A_hold(WDT_A_BASE);

    //Set P1.0 to output direction
    GPIO_setAsOutputPin(
        GPIO_PORT_P1,
        GPIO_PIN0
        );

    //Start timer in continuous mode sourced by SMCLK
    Timer_A_initContinuousModeParam initContParam = {0};
    initContParam.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    initContParam.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    initContParam.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
    initContParam.timerClear = TIMER_A_DO_CLEAR;
    initContParam.startTimer = false;
    Timer_A_initContinuousMode(TIMER_A1_BASE, &initContParam);

    //Initiaze compare mode
    Timer_A_clearCaptureCompareInterrupt(TIMER_A1_BASE,
        TIMER_A_CAPTURECOMPARE_REGISTER_0
        );

    Timer_A_initCompareModeParam initCompParam = {0};
    initCompParam.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_0;
    initCompParam.compareInterruptEnable = TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
    initCompParam.compareOutputMode = TIMER_A_OUTPUTMODE_OUTBITVALUE;
    initCompParam.compareValue = COMPARE_VALUE;
    Timer_A_initCompareMode(TIMER_A1_BASE, &initCompParam);

    Timer_A_startCounter( TIMER_A1_BASE,
            TIMER_A_CONTINUOUS_MODE
                );
}

void calcValues()
{
    float a = 1445;
    float b = -3.87;
    float c = 0.8176;
    float temp1;

    //S_Inom = 6; // Set nominal current to 6 Amps
    S_Inom_A = S_Inom*DA_Current;

    temp1 = S_Vmax*DA_Voltage;
    S_Vmax_A = (int)temp1;

    temp1 = S_Isc*DA_Current;
    S_Isc_A = (int)temp1;

    tempStart = (1.1*S_Inom*DA_Current)*4096; // Factor 4096 (or << 12)
    tempStep = (0.02*S_Inom*DA_Current)*4096;

    startCurve_A = (long)(tempStart);
    stepCurve_A = (long)(tempStep);

    tempResult = 0;
    allowedValue = 0;

    float temp = 1.1;
    int i2 = 0; // used to loop

    // Sample characteristics
    while(i2 < 1024)        // Sample curve -- Every array step: 0.02*Inom
    {
        tripCurve[i2] = (float)((a*pow(temp,b)+c)*1000); // convert seconds to milliseconds
        temp = temp + 0.02;

        if((i2%5) == 0)
        {
            GPIO_toggleOutputOnPin(GPIO_PORT_P4, GPIO_PIN7);
        }
        i2++;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC12_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(ADC12_VECTOR)))
#endif
void ADC12ISR (void)
{
    switch (__even_in_range(ADC12IV,34)){
        case  0: break;   //Vector  0:  No interrupt
        case  2: break;   //Vector  2:  ADC overflow
        case  4: break;   //Vector  4:  ADC timing overflow
        case  6: break;   //Vector  6:  ADC12IFG0
        case  8: break;   //Vector  8:  ADC12IFG1
        case 10: break;   //Vector 10:  ADC12IFG2
        case 12:          //Vector 12:  ADC12IFG3

// ===================== READ ADC'S ======================

        //Move A0 results, IFG is cleared
        A0result =
            ADC12_A_getResults(ADC12_A_BASE,
                ADC12_A_MEMORY_0);
        //Move A1 results, IFG is cleared
        A1result =
            ADC12_A_getResults(ADC12_A_BASE,
                ADC12_A_MEMORY_1);
        //Move A2 results, IFG is cleared
        A2result =
            ADC12_A_getResults(ADC12_A_BASE,
                ADC12_A_MEMORY_2);

// =================== PROCESS VALUES ====================

        M_Iin_A = abs(A0result-2048);
        M_Iout_A = abs(A1result-2048);
        M_Ileak_A = abs(A0result - A1result);
        M_Vin_A = A2result;
        if (A0result < 2048)
        {
            negative = 1;
        }
        else
        {
            negative = 0;
        }

// ==================== SAFETY CHECKS ====================
        /* when system is active CAL_DATA[0] is 0xFFF1
         * CAL_DATA[0] is on initalisation 0XFFFF
         * CAL_DATA[0] = 0xFFF6 lekstroom schrijf daarna write_infoB();
         * CAL_DATA[0] = 0xFFF5 overspanning schrijf daarna write_infoB();
         * CAL_DATA[0] = 0xFFF4 overbelasting schrijf daarna write_infoB();
         * CAL_DATA[0] = 0xFFF3 short circuit schrijf daarna write_infoB();
         * CAL_DATA[0] = 0xFFF2 System deactivated schrijf daarna write_infoB();
         */
        if (M_Iin_A > S_Isc_A)
        {
            GPIO_setOutputHighOnPin(
                    GPIO_PORT_P6,
                    GPIO_PIN7
                    ); // open switch
            //CAL_DATA[0] = 0xFFF3;
        }

        if (M_Ileak_A > S_Ileak_A)
        {

            GPIO_setOutputHighOnPin(
                    GPIO_PORT_P6,
                    GPIO_PIN7
                    ); // open switch
            //CAL_DATA[0] = 0xFFF6;
        }

        if (M_Vin_A > S_Vmax_A)
        {

            GPIO_setOutputHighOnPin(
                    GPIO_PORT_P6,
                    GPIO_PIN7
                    );
            //CAL_DATA[0] = 0xFFF5;
        }

        else
        {
            GPIO_setOutputLowOnPin(
                    GPIO_PORT_P6,
                    GPIO_PIN7
                    );
        }

// ==================== STORE RESULTS ====================

        static int loop = 0;

        buffer [loop] = A0result;
        loop ++;
        if (loop > 1023)
        {
            loop = 0;
        }


// =======================================================
        case 14: break;   //Vector 14:  ADC12IFG4
        case 16: break;   //Vector 16:  ADC12IFG5
        case 18: break;   //Vector 18:  ADC12IFG6
        case 20: break;   //Vector 20:  ADC12IFG7
        case 22: break;   //Vector 22:  ADC12IFG8
        case 24: break;   //Vector 24:  ADC12IFG9
        case 26: break;   //Vector 26:  ADC12IFG10
        case 28: break;   //Vector 28:  ADC12IFG11
        case 30: break;   //Vector 30:  ADC12IFG12
        case 32: break;   //Vector 32:  ADC12IFG13
        case 34: break;   //Vector 34:  ADC12IFG14
        default: break;
    }

}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER1_A0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER1_A0_VECTOR)))
#endif
void TIMER1_A0_ISR (void)
{

    static uint32_t milliseconds = 0;    // Time in trip
    static int tracer = 0;          // Tracks position in curve related to milliseconds in trip
    static int divider = 0;         // Divider for milliseconds
    static int integrator = 0;
    static int c_ping = 0;
    static int c_send = 0;

    uint16_t compVal = Timer_A_getCaptureCompareCount(TIMER_A1_BASE,
            TIMER_A_CAPTURECOMPARE_REGISTER_0)
            + COMPARE_VALUE;

    ADC12_A_startConversion(ADC12_A_BASE,
        ADC12_A_MEMORY_0,
        ADC12_A_SEQOFCHANNELS);

    divider ++;

    if (divider >= 4)
    {
        milliseconds ++;
        divider = 0;
        //Toggle P1.0 (LED)
//        GPIO_toggleOutputOnPin(
//            GPIO_PORT_P1,
//            GPIO_PIN0
//            );

        //keeps track of the milliseconds for the wifi transmition
        c_ping++;
        c_send++;
    }

    if(c_ping >= 10000)
    {
        ping_bit = 1;
        c_ping = 0;
    }

    if(c_send >= 60000)
    {
        send_bit = 1;
        c_send = 0;
    }
    // Values used by binary search loop
    uint16_t middle = 512;
    uint16_t div = 512;
    uint8_t loop; // Used by binary search loop
    for (loop = 0 ; loop < 9 ; loop ++)
    {
        if (tripCurve[middle] == milliseconds)
        {
            loop = 9;
        }

        if (tripCurve[middle] > milliseconds)
        {
            middle = middle + (div = div >> 1);
        }

        else
        {
            middle = middle - (div = div >> 1);
        }

        tracer = middle;
    }

    if (M_Iin_A > S_Inom_A)
    {
        // Calculate allowed current related to the time in trip
        tempResult = (long long)(startCurve_A+(stepCurve_A*tracer)); // Allowed value (fixed point (*4096))
        allowedValue = (tempResult >> 12); // The allowed current (in Analog value) related to time in tripCurve

        // Check Tripcurve for overload
        //int compare = (1.1+(tracer*0,02))*S_Inom_A;
        if ((M_Iin_A > allowedValue) || (milliseconds > 100000))
        {
            integrator ++;

            if (integrator > 10)
            {
                GPIO_setOutputHighOnPin(
                    GPIO_PORT_P1,
                    GPIO_PIN0
                    );
                // Overload
                // Open switch
            }
        }
        else
        {
            if (integrator > 0)
            integrator --;
        }
    }

    else
    {
        GPIO_setOutputLowOnPin(
                GPIO_PORT_P1,
                GPIO_PIN0
                );

        if (milliseconds > 0)
        {
            milliseconds --;
        }
    }

    //Add Offset to CCR0
    Timer_A_setCompareValue(TIMER_A1_BASE,
        TIMER_A_CAPTURECOMPARE_REGISTER_0,
        compVal
        );
}

static _i16 transport_getdata(unsigned char* buf, int count)
{
    int rc = sl_Recv(sockID, buf, count, 0);
    return rc;
}

static _i32 mqtt_connect(SlSockAddr_t * addr, MQTTPacket_connectData * data)
{
    _i32 retVal = -1;
    _i16 AddrSize = sizeof(SlSockAddrIn_t);
    unsigned char buf[200];
    int buflen = sizeof(buf);

    /* connect to the given host */
    retVal =  sl_Connect(sockID, addr, AddrSize);
    if(retVal < 0)
        return retVal;

    /* send a connect request to the broker */
    int connectMsg = MQTTSerialize_connect(buf, buflen, data); /* 1 */

    retVal = sl_Send(sockID, buf, connectMsg, NULL);
    if(retVal < 0)
        return retVal;

    /* wait for connection acknowledge from the broker */
    if (MQTTPacket_read(buf, buflen, transport_getdata) == CONNACK)
    {
        unsigned char sessionPresent, connack_rc;

        if (MQTTDeserialize_connack(&sessionPresent, &connack_rc, buf, buflen) != 1 || connack_rc != 0)
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }

    /* return 0 in case of success */
    return 0;
}

int TLSConnectNetwork(SlSockAddr_t * sAddr, unsigned char sec_method, unsigned int cipher)
{

    int addrSize;
    int retVal;

    addrSize = sizeof(SlSockAddrIn_t);

    sockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, SL_SEC_SOCKET);
    if (sockID < 0) {
        return -1;
    }

    SlSockSecureMethod method;
    method.secureMethod = sec_method;
    retVal = sl_SetSockOpt(sockID, SL_SOL_SOCKET, SL_SO_SECMETHOD, &method, sizeof(method));
    if (retVal < 0) {
        return retVal;
    }

    SlSockSecureMask mask;
    mask.secureMask = cipher;
    retVal = sl_SetSockOpt(sockID, SL_SOL_SOCKET, SL_SO_SECURE_MASK, &mask, sizeof(mask));
    if (retVal < 0) {
        return retVal;
    }


    retVal = sl_SetSockOpt(sockID, SL_SOL_SOCKET, SL_SO_SECURE_FILES_CA_FILE_NAME, SL_SSL_CA_CERT, strlen(SL_SSL_CA_CERT));
    if(retVal < 0)
    {
        return retVal;
    }


    retVal = sl_Connect(sockID, sAddr, addrSize);
    if( retVal < 0 ) {
        return retVal;
    }

    return retVal;
}

static _i32 mqtt_publish(MQTTString topicString, unsigned char* payload)
{
    _i32   retVal = -1;
    unsigned char buf[200];
    int    buflen = sizeof(buf);
    int    payloadlen = strlen((const char*)payload);

    int publishMsg = MQTTSerialize_publish(buf, buflen, NULL, 0, NULL, 0, topicString, payload, payloadlen);
    retVal = sl_Send(sockID, buf, publishMsg, NULL);

    return retVal;
}

static _i32 mqtt_subscribe(MQTTString * topicString, int * req_qos, int msgid)
{
    unsigned char buf[200];
    int    buflen = sizeof(buf);
    _i32   retVal = -1;

    int subscribeMsg = MQTTSerialize_subscribe(buf, buflen, 0, msgid, 1, topicString, req_qos);

    retVal = sl_Send(sockID, buf, subscribeMsg, 0);
    if(retVal < 0)
    {

        return -1;
    }
    if (MQTTPacket_read(buf, buflen, transport_getdata) == SUBACK)
    {
        unsigned short submsgid;
        int subcount;
        int granted_qos;

        retVal = MQTTDeserialize_suback(&submsgid, 1, &subcount, &granted_qos, buf, buflen);
        if (granted_qos != 0)
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }

    return 0;
}

static _i32 mqtt_disconnect()
{
    _i32            retVal = -1;
    unsigned char   buf[200];
    int             buflen = sizeof(buf);

    int closeMsg = MQTTSerialize_disconnect(buf, buflen);

    retVal = sl_Send(sockID, buf, closeMsg, NULL);
    if(retVal < 0 )
        return retVal;

    retVal = sl_Close(sockID);

    return retVal;
}

static _i32 mqtt_ping()
{
    unsigned char buf[200];
    int buflen = sizeof(buf);
    int retVal = -1;

    int pingMsg = MQTTSerialize_pingreq(buf, buflen);
    retVal = sl_Send(sockID, buf, pingMsg, 0);
    if(retVal < 0)
    {
        return -1;
    }

    int Return = MQTTPacket_read(buf, buflen, transport_getdata);
    if (Return == PINGRESP)
        return 0;

    if (Return == PUBLISH)
        return 1;

    else
        return -1;
}

/*!
    \brief This function configure the SimpleLink device in its default state. It:
           - Sets the mode to STATION
           - Configures connection policy to Auto and AutoSmartConfig
           - Deletes all the stored profiles
           - Enables DHCP
           - Disables Scan policy
           - Sets Tx power to maximum
           - Sets power policy to normal
           - Unregisters mDNS services
           - Remove all filters

    \param[in]      none

    \return         On success, zero is returned. On error, negative is returned
 */
static _i32 configureSimpleLinkToDefaultState()
{
    SlVersionFull   ver = {0};
    _WlanRxFilterOperationCommandBuff_t  RxFilterIdMask = {0};

    _u8           val = 1;
    _u8           configOpt = 0;
    _u8           configLen = 0;
    _u8           power = 0;

    _i32          retVal = -1;
    _i32          mode = -1;

    mode = sl_Start(0, 0, 0);
    ASSERT_ON_ERROR(mode);

    /* If the device is not in station-mode, try configuring it in station-mode */
    if (ROLE_STA != mode)
    {
        if (ROLE_AP == mode)
        {
            /* If the device is in AP mode, we need to wait for this event before doing anything */
            while(!IS_IP_ACQUIRED(g_Status)) { _SlNonOsMainLoopTask(); }
        }

        /* Switch to STA role and restart */
        retVal = sl_WlanSetMode(ROLE_STA);
        ASSERT_ON_ERROR(retVal);

        retVal = sl_Stop(SL_STOP_TIMEOUT);
        ASSERT_ON_ERROR(retVal);

        retVal = sl_Start(0, 0, 0);
        ASSERT_ON_ERROR(retVal);

        /* Check if the device is in station again */
        if (ROLE_STA != retVal)
        {
            /* We don't want to proceed if the device is not coming up in station-mode */
            ASSERT_ON_ERROR(DEVICE_NOT_IN_STATION_MODE);
        }
    }

    /* Get the device's version-information */
    configOpt = SL_DEVICE_GENERAL_VERSION;
    configLen = sizeof(ver);
    retVal = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &configOpt, &configLen, (_u8 *)(&ver));
    ASSERT_ON_ERROR(retVal);

    /* Set connection policy to Auto + SmartConfig (Device's default connection policy) */
    retVal = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(1, 0, 0, 0, 1), NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Remove all profiles */
    retVal = sl_WlanProfileDel(0xFF);
    ASSERT_ON_ERROR(retVal);

    /*
     * Device in station-mode. Disconnect previous connection if any
     * The function returns 0 if 'Disconnected done', negative number if already disconnected
     * Wait for 'disconnection' event if 0 is returned, Ignore other return-codes
     */
    retVal = sl_WlanDisconnect();
    if(0 == retVal)
    {
        /* Wait */
        while(IS_CONNECTED(g_Status)) { _SlNonOsMainLoopTask(); }
    }

    /* Enable DHCP client*/
    retVal = sl_NetCfgSet(SL_IPV4_STA_P2P_CL_DHCP_ENABLE,1,1,&val);
    ASSERT_ON_ERROR(retVal);

    /* Disable scan */
    configOpt = SL_SCAN_POLICY(0);
    retVal = sl_WlanPolicySet(SL_POLICY_SCAN , configOpt, NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Set Tx power level for station mode
       Number between 0-15, as dB offset from max power - 0 will set maximum power */
    power = 0;
    retVal = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, WLAN_GENERAL_PARAM_OPT_STA_TX_POWER, 1, (_u8 *)&power);
    ASSERT_ON_ERROR(retVal);

    /* Set PM policy to normal */
    retVal = sl_WlanPolicySet(SL_POLICY_PM , SL_NORMAL_POLICY, NULL, 0);
    ASSERT_ON_ERROR(retVal);

    /* Unregister mDNS services */
    retVal = sl_NetAppMDNSUnRegisterService(0, 0);
    ASSERT_ON_ERROR(retVal);

    /* Remove  all 64 filters (8*8) */
    pal_Memset(RxFilterIdMask.FilterIdMask, 0xFF, 8);
    retVal = sl_WlanRxFilterSet(SL_REMOVE_RX_FILTER, (_u8 *)&RxFilterIdMask,
                                sizeof(_WlanRxFilterOperationCommandBuff_t));
    ASSERT_ON_ERROR(retVal);

    retVal = sl_Stop(SL_STOP_TIMEOUT);
    ASSERT_ON_ERROR(retVal);

    retVal = initializeAppVariables();
    ASSERT_ON_ERROR(retVal);

    return retVal; /* 0 = Success */
}

/*!
    \brief Connecting to a WLAN Access point

    This function connects to the required AP (SSID_NAME).
    The function will return once we are connected and have acquired IP address

    \param[in]  None

    \return     0 on success, negative error-code on error

    \note

    \warning    If the WLAN connection fails or we don't acquire an IP address,
                We will be stuck in this function forever.
 */
static _i32 establishConnectionWithAP()
{
    SlSecParams_t secParams = {0};
    _i32 retVal = 0;

    secParams.Key = (_i8 *)PASSKEY;
    secParams.KeyLen = pal_Strlen(PASSKEY);
    secParams.Type = SEC_TYPE;

    retVal = sl_WlanConnect((_i8 *)SSID_NAME, pal_Strlen(SSID_NAME), 0, &secParams, 0);
    ASSERT_ON_ERROR(retVal);

    /* Wait */
    while((!IS_CONNECTED(g_Status)) || (!IS_IP_ACQUIRED(g_Status))) { _SlNonOsMainLoopTask(); }

    return SUCCESS;
}

/*!
    \brief This function checks the LAN connection by pinging the AP's gateway

    \param[in]  None

    \return     0 on success, negative error-code on error
 */
static _i32 checkLanConnection()
{
    SlPingStartCommand_t pingParams = {0};
    SlPingReport_t pingReport = {0};

    _i32 retVal = -1;

    CLR_STATUS_BIT(g_Status, STATUS_BIT_PING_DONE);
    g_PingPacketsRecv = 0;

    /* Set the ping parameters */
    pingParams.PingIntervalTime = PING_INTERVAL;
    pingParams.PingSize = PING_PKT_SIZE;
    pingParams.PingRequestTimeout = PING_TIMEOUT;
    pingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;
    pingParams.Flags = 0;
    pingParams.Ip = g_GatewayIP;

    /* Check for LAN connection */
    retVal = sl_NetAppPingStart( (SlPingStartCommand_t*)&pingParams, SL_AF_INET,
                                 (SlPingReport_t*)&pingReport, SimpleLinkPingReport);
    ASSERT_ON_ERROR(retVal);

    /* Wait */
    while(!IS_PING_DONE(g_Status)) { _SlNonOsMainLoopTask(); }

    if(0 == g_PingPacketsRecv)
    {
        /* Problem with LAN connection */
        ASSERT_ON_ERROR(LAN_CONNECTION_FAILED);
    }

    /* LAN connection is successful */
    return SUCCESS;
}

/*!
    \brief This function checks the internet connection by pinging
           the external-host (HOST_NAME)

    \param[in]  None

    \return     0 on success, negative error-code on error
 */
static _i32 checkInternetConnection()
{
    SlPingStartCommand_t pingParams = {0};
    SlPingReport_t pingReport = {0};

    _u32 ipAddr = 0;

    _i32 retVal = -1;

    CLR_STATUS_BIT(g_Status, STATUS_BIT_PING_DONE);
    g_PingPacketsRecv = 0;

    /* Set the ping parameters */
    pingParams.PingIntervalTime = PING_INTERVAL;
    pingParams.PingSize = PING_PKT_SIZE;
    pingParams.PingRequestTimeout = PING_TIMEOUT;
    pingParams.TotalNumberOfAttempts = NO_OF_ATTEMPTS;
    pingParams.Flags = 0;
    pingParams.Ip = g_GatewayIP;

    /* Check for Internet connection */
    retVal = sl_NetAppDnsGetHostByName((_i8 *)HOST_NAME, pal_Strlen(HOST_NAME), &ipAddr, SL_AF_INET);
    ASSERT_ON_ERROR(retVal);

    /* Replace the ping address to match HOST_NAME's IP address */
    pingParams.Ip = ipAddr;

    /* Try to ping HOST_NAME */
    retVal = sl_NetAppPingStart( (SlPingStartCommand_t*)&pingParams, SL_AF_INET,
                                 (SlPingReport_t*)&pingReport, SimpleLinkPingReport);
    ASSERT_ON_ERROR(retVal);

    /* Wait */
    while(!IS_PING_DONE(g_Status)) { _SlNonOsMainLoopTask(); }

    if (0 == g_PingPacketsRecv)
    {
        /* Problem with internet connection*/
        ASSERT_ON_ERROR(INTERNET_CONNECTION_FAILED);
    }

    /* Internet connection is successful */
    return SUCCESS;
}

static void SimpleLinkPingReport(SlPingReport_t *pPingReport)
{
    SET_STATUS_BIT(g_Status, STATUS_BIT_PING_DONE);

    if(pPingReport == NULL)
    {
        return;
    }

    g_PingPacketsRecv = pPingReport->PacketsReceived;
}

/*!
    \brief This function initializes the application variables

    \param[in]  None

    \return     0 on success, negative error-code on error
 */
static _i32 initializeAppVariables()
{
    g_Status = 0;
    g_PingPacketsRecv = 0;
    g_GatewayIP = 0;

    return SUCCESS;
}
